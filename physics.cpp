// FW::Vec3f contains floats in x, y and z
typedef std::vector<FW::Vec3f> State;
typedef std::vector<FW::Vec3f> Points;
typedef std::vector<FW::Vec3f> Lines;

using namespace std;
using namespace FW;

namespace {

inline Vec3f fGravity(float mass) {
    return Vec3f(0, -9.8f * mass, 0);
}

// force acting on particle at pos1 due to spring attached to pos2 at the other end
inline Vec3f fSpring(const Vec3f& pos1, const Vec3f& pos2, float k, float rest_length) {
    // YOUR CODE HERE (R2)
    Vec3f d(pos1-pos2);
    return Vec3f(-k * (d.length() - rest_length) * (d / d.length()));
}

inline Vec3f fDrag(const Vec3f& v, float k) {
    // YOUR CODE HERE (R2)
    return Vec3f(-k * v);
}

inline Vec3f fWind(Vec3f& normal, float wind_speed) {
    // EXTRA
    Vec3f dat_wind(0, 0, wind_speed);
    return dot(dat_wind, normal) * normal;
}

} // namespace


// Spring system

// Create system and set the initial state
void SpringSystem::reset() {
    const auto start_pos = Vec3f(0.1f, -0.5f, 0.0f);
    const auto spring_k = 30.0f;
    state_ = State(4);
    // YOUR CODE HERE (R2)
    // Set the initial state for a particle system with one particle fixed
    // at origin and another particle hanging off the first one with a spring.
    // Place the second particle initially at start_pos.

    // States are stored as follows: state[0] is the position of the first particle.
    // State[1] is the force affecting the first particle.
    // state[2] is the position of the second particle and so on.
    state_[0] = Vec3f(0.0f, 0.0f, 0.0f);
    state_[1] = Vec3f(0.0f, 0.0f, 0.0f);
    state_[2] = start_pos;
    state_[3] = Vec3f(0.0f, 0.0f, 0.0f);

    spring_.k = spring_k;
    spring_.rlen = start_pos.length();
}

// Evaluate force and set the state accordingly
State SpringSystem::evalF(const State& state) const {
    const auto drag_k = 0.5f;
    const auto mass = 1.0f;
    State f(4);
    // YOUR CODE HERE (R2)
    // Return a derivative for the system as if it was in state "state".
    // You can use the fGravity, fDrag and fSpring helper functions for the forces.
    Vec3f g = fGravity(mass);
    Vec3f d = fDrag(state[3], drag_k);
    Vec3f s = fSpring(state[2], state[0], spring_.k, spring_.rlen);
    Vec3f force = (g + d + s);
    f[0] = state[0];
    f[1] = state[1];
    f[2] = state[3];
    f[3] = force / mass;

    return f;
}

Points SpringSystem::getPoints() {
    auto p = Points(2);
    p[0] = state_[0]; p[1] = state_[2];
    return p;
}

Lines SpringSystem::getLines() {
    auto l = Lines(2);
    l[0] = state_[0]; l[1] = state_[2];
    return l;
}


// Cloth system

// Create system and set the initial state
void ClothSystem::reset() {
    const auto spring_k = 300.0f;
    const auto width = 1.5f, height = 1.5f; // width and height of the whole grid
    state_ = State(2*x_*y_);
    // YOUR CODE HERE (R5)
    // Construct a particle system with a x_ * y_ grid of particles,
    // connected with a variety of springs as described in the handout:
    // structural springs, shear springs and flex springs.

    Vec3f startPoint(-width / 2, 0.0f, 0.0f);
    Vec3f midVectorX = Vec3f(width / x_, 0.0f, 0.0f);
    Vec3f midVectorY = Vec3f(0.0f, 0.0f, height / y_);
    float distance = midVectorX.length();
    unsigned posIdx = 0, nodeIdx = 0; // Current state position index and Current index of the node
    float shearDistance = (midVectorX + midVectorY).length();
    float flexDistance = distance * 2;

    for (unsigned y = 0; y < y_; y++) {
        for (unsigned x = 0; x < x_; x++) {
            // Node positions and velocities
            state_[posIdx] = startPoint + (midVectorX * x); // Positions
            state_[posIdx + 1] = Vec3f(0); // Velocities to zero
            // Springs
            // Structural x axis
            if (x < x_ - 1)
                springs_.push_back(Spring(nodeIdx, nodeIdx + 1, spring_k, distance));
            // Structural y axis
            if (y < y_ - 1)
                springs_.push_back(Spring(nodeIdx, nodeIdx + x_, spring_k, distance));
            // Shear to lower right
            if (x < x_ - 1 && y < y_ - 1) 
                springs_.push_back(Spring(nodeIdx, nodeIdx + 1 + x_, spring_k, shearDistance));
            // Shear to lower left
            if (x > 0 && y < y_ - 1)
                springs_.push_back(Spring(nodeIdx, nodeIdx - 1 + x_, spring_k, shearDistance));
            // Flex x axis
            if (x < x_ - 2)
                springs_.push_back(Spring(nodeIdx, nodeIdx + 2, spring_k, flexDistance));
            // Flex y axis
            if (y < y_ - 2)
                springs_.push_back(Spring(nodeIdx, nodeIdx + (x_ * 2), spring_k, flexDistance));
            // Update indices
            nodeIdx++; posIdx = nodeIdx * 2;
        }
        startPoint -= midVectorY;
    }
}

// Evaluate force and set the state accordingly
State ClothSystem::evalF(const State& state) const {
    const auto drag_k = 0.08f;
    const auto n = x_ * y_;
    static const auto mass = 0.025f;
    auto f = State(2*n);
    // YOUR CODE HERE (R5)
    // This will be much like in R2 and R4.

    // Forces of strings
    for (auto spring : springs_) {
        Vec3f force = fSpring(state[spring.i1 * 2], state[spring.i2 * 2], spring.k, spring.rlen);
        f[spring.i1 * 2 + 1] += force / mass;
        f[spring.i2 * 2 + 1] -= force / mass;
    }
    
    for (unsigned i = 0; i < n; i++) {
        if (i == 0 || i == (x_ - 1)) { // The hanging points
            f[i * 2] = Vec3f(0);
            f[i * 2 + 1] = Vec3f(0);
            continue;
        }
        // Forces of gravity and drag:
        f[i * 2] = state[i * 2 + 1];
        Vec3f g = fGravity(mass);
        Vec3f d = fDrag(state[i * 2 + 1], drag_k);
        // Force of wind:
        Vec3f w(0);
        if (wind_toggle_) {
            Vec3f n0(state[i * 2]), n1(0), n2(0), n3(0), n4(0);
            if (i % x_ != 0) // Left
                n3 = state[(i - 1) * 2];
            if (i % x_ != x_ - 1) // Right
                n1 = state[(i + 1) * 2];
            if (i >= x_) // Up
                n4 = state[(i - x_) * 2];
            if (i < x_ * (y_ - 1)) // Down
                n2 = state[(i + x_) * 2];
            w = fWind(getNormal(n0, n1, n2, n3, n4), wind_speed_);
        }
        Vec3f force = g + d + w;
        f[i * 2 + 1] += force / mass;
    }
    return f;
}

Vec3f ClothSystem::getNormal(Vec3f& node0, Vec3f& node1, Vec3f& node2, Vec3f& node3, Vec3f& node4) const {
    Vec3f normal(0);
    normal += cross(Vec3f(-node0 + node1), Vec3f(-node0 + node2));
    normal += cross(Vec3f(-node0 + node2), Vec3f(-node0 + node3));
    normal += cross(Vec3f(-node0 + node3), Vec3f(-node0 + node4));
    normal += cross(Vec3f(-node0 + node4), Vec3f(-node0 + node1));
    int a = 0;
    for (Vec3f vec : vector<Vec3f>({ node1, node2, node3, node4 })) {
        if (vec != Vec3f(0)) a++;
    }
    return (normal / a).normalized();
}

Points ClothSystem::getPoints() {
    auto n = x_ * y_;
    auto p = Points(n);
    for (auto i = 0u; i < n; ++i) {
        p[i] = state_[2*i];
    }
    return p;
}

Lines ClothSystem::getLines() {
    auto l = Lines();
    for (const auto& s : springs_) {
        l.push_back(state_[2*s.i1]);
        l.push_back(state_[2*s.i2]);
    }
    return l;
}